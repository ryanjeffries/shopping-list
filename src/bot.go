package main

import (
	"time"
	"fmt"
	"strings"
	"gitlab.com/ryanjeffries/telegram-bot/telegrambot"
	"strconv"
	"bytes"
)

const DELAY = 1

func subscriber(token string) {
	var delay = DELAY * time.Second

	var offset = telegrambot.GetCurrentOffset(token)
	for {
		var updates = telegrambot.GetUpdates(token, telegrambot.GetUpdateMethodRequest{Offset: offset})
		offset = processUpdates(updates, offset, token)
		time.Sleep(delay)
	}
}

func processUpdates(updates []telegrambot.Update, offset int, token string) int {
	var newOffset = offset
	for _, update := range updates {
		if update.Message != nil {
			newOffset = update.UpdateID + 1
			if update.Message != nil {
				commandRouter(strconv.Itoa(update.Message.Chat.ID), getInput(update))
			}
		}
	}
	return newOffset
}

var shoppingList = ShoppingList{Items: make(map[string]int)}

func getInput(update telegrambot.Update) []string {
	var input = strings.Fields(update.Message.Text)
	return input
}

func commandRouter(chatId string, input[] string) {
	if (len(input) >= 1) {
		var command = input[0]
		switch (command) {
		case "/add":
			add(shoppingList, input[1:])
			break
		case "/remove":
			remove(shoppingList, input[1:], chatId)
			break
		case "/removeall":
			removeAll(shoppingList, chatId)
			hideList(chatId, "All items have been deleted")
			break
		case "/hidelist":
			hideList(chatId, "Closed List")
			break
		case "/list":
			list(chatId)
			break
		default:
			if (remove(shoppingList, input, chatId)) {
				if (len(shoppingList.Items) == 0) {
					hideList(chatId, "Empty List")
				} else {
					removeMarkdown(chatId)
				}
			}
			break
		}
	}
}

func hideList(chatId, message string) {
	var replyKeyboard = telegrambot.ReplyKeyboardHide{HideKeyboard: true, Selective: true}

	telegrambot.SendMethodRequest(token, telegrambot.SendMessageMethodRequest{Text: message, ReplyMarkup: replyKeyboard, ChatId: chatId})
}

func list(chatId string) {
	var buffer bytes.Buffer
	buffer.WriteString("List:\n")
	var count = 1
	for key, value := range shoppingList.Items {
		buffer.WriteString(fmt.Sprintf("%d)   %d   %s\n", count, value, key))
		count++
	}
	telegrambot.SendMethodRequest(token, telegrambot.SendMessageMethodRequest{Text: buffer.String(), ChatId: chatId})
}

func removeMarkdown(chatId string) {
	keyboard := [][]telegrambot.KeyboardButton{}

	hideButton := []telegrambot.KeyboardButton{telegrambot.KeyboardButton{Text: "/hidelist", RequestContact: false, RequestLocation: false}}
	keyboard = append(keyboard, hideButton)

	for key, value := range shoppingList.Items {
		itemButton := []telegrambot.KeyboardButton{telegrambot.KeyboardButton{Text: fmt.Sprintf("%d %s", value, key), RequestContact: false, RequestLocation: false}}
		keyboard = append(keyboard, itemButton)
	}
	removeAllButton := []telegrambot.KeyboardButton{telegrambot.KeyboardButton{Text: "/removeall", RequestContact: false, RequestLocation: false}}
	keyboard = append(keyboard, removeAllButton)

	var replyKeyboard = telegrambot.ReplyKeyboardMarkup{Keyboard: keyboard, ResizeKeyboard: true, OneTimeKeyboard: false, Selective: true}
	telegrambot.SendMethodRequest(token, telegrambot.SendMessageMethodRequest{Text: "Which item would you like to remove?:", ReplyMarkup: replyKeyboard, ChatId:chatId})
}