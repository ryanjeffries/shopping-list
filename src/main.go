package main

import (
	"gitlab.com/ryanjeffries/shopping-list-bot-secrets/shoppingListBotSecrets"
	"gitlab.com/ryanjeffries/telegram-bot/telegrambot"
)

var token = shoppingListBotSecrets.GetSecrets().Token

func main() {
	go subscriber(token)
	telegrambot.HandleRequests(&telegrambot.Address{Port: "8443", Ip: "", Token: token})
}