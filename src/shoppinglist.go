package main

import (
	"strconv"
)

type ShoppingList struct {
	Items map[string]int
}

func add(list ShoppingList, input[] string) {
	quantity, err := strconv.ParseInt(input[0], 10, 0)
	if (err != nil) {
		list.Items[input[0]] += 1
	} else {
		if (len(input) > 1) {
			list.Items[input[1]] += int(quantity)
		}
	}
}

func remove(list ShoppingList, input[] string, chatId string) bool {
	if (len(input) == 0) {
		removeMarkdown(chatId)
		return true
	} else {
		quantity, err := strconv.ParseInt(input[0], 10, 0)
		if (err != nil) {
			delete(list.Items, input[0])
			return true
		} else {
			if (len(input) > 1) {
				removeItem(list, input[1], int(quantity))
				return true
			}
		}
	}
	return false
}

func removeItem(list ShoppingList, item string, amount int) {
	if _, ok := list.Items[item]; ok {
		if (list.Items[item] - amount <= 0) {
			delete(list.Items, item)
		} else {
			list.Items[item] = list.Items[item] - amount
		}
	}
}

func removeAll(list ShoppingList, chatId string) {
	list.Items = make(map[string]int)
}